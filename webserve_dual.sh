#!/bin/bash
if (( $# > 0 ))
then
    # hostname is needed for this
    hostname=$1
else
    echo "WTF! Supply a hostname/IP address" 1>&2
    exit 1
fi

ssh -o StrictHostKeyChecking=no -i ~/.ssh/JamaineObiri-YeboahKey.pem ec2-user@$hostname '
sudo yum -y install httpd
if rpm -qa | grep 'httpd-[0-9]' >/dev/null 2>&1
then
    sudo systemctl start httpd
else
    exit 240
fi

sudo sh -c "cat >/var/www/html/index.html << _END_
<body style=\"background-color:blue;\">
<h1>HELLO WORLD</h1>
<h3>I learnt from Filipe! Refresh for a new image</h3>
<img src=\"https://source.unsplash.com/random\" height=\"600\">
_END_"
'